import axios from "axios";
import type { Address } from "./entities";


export async function fetchAllAddresses() {
    const response = await axios.get<Address[]>('/api/address');
    return response.data;
}


export async function deleteAddress(id:any) {
    await axios.delete<void>('/api/address/'+id);
    
}