export interface User {
    id?:number;
    email:string;
    password?:string;
    role?:string;
}

export interface Address {
    id?:number;
    street:string;
    city:string;
    owner:User
}