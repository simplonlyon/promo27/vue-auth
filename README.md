# vue-auth
Application VueJS pour voir l'authentification, liée à [cette application Spring Boot](https://gitlab.com/simplonlyon/promo27/spring-auth-jdbc)

## Dépendance utilisées
* VueJS
* Vue Router (pour faire des pages)
* Pinia (pour la gestion des ref globales, ici on s'en sert pour stocker le user)
* Axios (pour les requêtes HTTP vers le back)

## L'Authentification

### Configuration de axios
On crée un fichier de configuration de axios pour faire que toutes les requêtes faites par axios dans cette application utilisent ces paramètres.

```ts
//Pour envoyer et récupérer les cookies de sessions avec le backend
axios.defaults.withCredentials = true;
//Pour faire qu'en ne soit pas obligé d'écrire l'url du serveur pour chaque requête effectuées
axios.defaults.baseURL = 'http://localhost:8080';
//Pour indiquer à Spring que les requêtes sont faites par JS et changer certains comportement par défaut
axios.defaults.headers.common['X-Requested-With'] ='XMLHttpRequest';
```

### Le Auth Service
Un service avec les différents appels nécessaires pour l'authentification, seuls particularité, pour la route de login on met l'identifiant et le mot de passe dans les entêtes de la requête. 

```ts
export async function fetchLogin(email:string,password:string) {
    const response = await axios.get<User>('/api/account', {
        auth: {
            username: email,
            password
        }
    });
    return response.data

}
export async function fetchLogout() {
    await axios.get<void>('/logout');
}
```

### Le Store Pinia pour le User
Pour avoir une variable User accessible dans n'importe quel component/pages, on utilise Pinia qui permet de définir des "stores" dans lequels on peut déclarer des ref et des fonctions qui pourront être importées et utiliser dans n'importe quel composant.

```ts
export const useAuth = defineStore('auth', () => {
    //La ref User qui contiendra le user connecté, ou rien
    const user = ref<User>();
    //Au lancement du store, on récupère dans le localStorage un éventuel user précédemment stocké
    const stored = localStorage.getItem('user');
    //S'il y avait en effet un user en localStorage
    if(stored) {
        //On parse son JSON et on le met en valeur de la ref
        user.value = JSON.parse(stored);
    }

    /**
     * Fonction qui va lancer la requête de login vers le back et assigné le User
     * récupéré en valeur de la ref ainsi que stocker ce User en chaîne de caractères
     * JSON dans le localStorage
     */
    async function login(email:string,password:string) {
        const data = await fetchLogin(email,password);
        localStorage.setItem('user', JSON.stringify(data));
        user.value = data;
    }
    /**
     * Fonction qui appel la route de logout du backend et supprime le user du 
     * localStorage ainsi que de la ref
     */
    async function logout() {
        await fetchLogout();
        localStorage.removeItem('user');
        user.value = undefined;
    }
    //Ici on expose les variables et fonctions qui seront disponibles dans les composants
    return {user,login,logout};
})

Pour se servir du store dans un component on peut faire :
```vue
<script setup lang="ts">
import {useAuth} from '@/stores/auth';
const auth = useAuth();
</script>
<template>
    <p v-if="auth.user">
        Vous êtes connecté⋅e en tant que {{auth.user.email}}
        <button @click="auth.logout()">Logout</button>
    </p>
</template>
```

### Formulaire de Login
Pour pouvoir se connecter, on créer une page et/ou un component avec un formulaire de Login. Celui ci devra lancer la méthode login du store d'authentification. 

[Exemple d'implémentation de formulaire](src/views/LoginView.vue)

### Redirection et deconnexion si session expirée
Il peut arriver que notre cookie/notre session de connexion expire (par défaut, les sessions n'existent que pour un temps donné, configurable côté backend), cela peut alors entraîner une situation où le front pense qu'on est connecté, car il récupère le user dans le localStorage, mais en réalité si on fait une requête vers le back, celui ci nous dira Unauthorized, car session expirée.

On peut rajouter dans le axios-config un intercepteur de réponse pour faire que axios vérifie toutes les réponses http de toutes les requêtes effectuées, et si jamais une de ces réponses contient un status 401 Unauthorized, alors on fait en sorte de supprimer le user du localStorage et du store ainsi que de rediriger vers la page de login.

```ts
axios.interceptors.response.use(null, (error) => {
    //Si l'erreur contient une réponse avec un status 401
    if(error.response.status == 401) {
        //On récupère le auth store et on lance le logout pour "déconnecter" le user
        const auth = useAuth();
        auth.logout();
        //et on redirige vers la page de login
        router.push('/login');
    }
    return Promise.reject(error);
});
```